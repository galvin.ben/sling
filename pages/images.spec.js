import { shallow } from 'enzyme'
import React from 'react'

import Images from './images.js'

describe('Check images page content', () => {
  it('Images page renders the name "Sling"', () => {
    const images = shallow(<Images />)
    expect(images.find('p').text()).toEqual('Images')
  })
})