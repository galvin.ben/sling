import { shallow } from 'enzyme'
import React from 'react'

import App from './index.js'

describe('Check index page content', () => {
  it('App shows the name "Sling"', () => {
    const app = shallow(<App />)
    expect(app.find('p').text()).toEqual('Sling')
  })
})