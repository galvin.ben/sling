import Header from '../components/Header'
import Head from 'next/head'

const Layout = (props) => (
  <div>
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />
    </Head>
    <Header />
    {props.children}
    <style jsx global>
      {`
        @font-face {
          font-family: 'roboto';
          src: url('./static/fonts/Roboto-Regular.ttf');
        }
        body {
          margin: 0;
          font-family: roboto
        }
      `}
    </style>
  </div>
)

export default Layout