# Sling

Sling is a git backend content management system for use with single page applications.

Why use a CDN or third party when you can commit all your images to your git repo along with all your other assets?

Initially created for a simple way for a website manager/maintainer to upload and manage a websites images with decent UX and without an overly complicated UI (eg. Contentful/other new headless CMS'). However, will be expanded to other content types and use cases in the near future.