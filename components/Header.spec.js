import { shallow } from 'enzyme'
import React from 'react'

import Header from './Header.js'

describe('<Header />', () => {
  it('renders the title', () => {
    const header = shallow(<Header />)
    expect(header.find('.header__title').text()).toEqual('Sling')
  })
  it('renders two links correctly', () => {
    const header = shallow(<Header />)
    expect(header.find('.header__link')).toHaveLength(2)
    expect(header.find('.header__link').at(0).text()).toEqual('About')
    expect(header.find('.header__link').at(1).text()).toEqual('Images')
    expect(header.find('Link').at(0).prop('href')).toEqual('/')
    expect(header.find('Link').at(1).prop('href')).toEqual('/images')
  })
})