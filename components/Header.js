import Link from 'next/link'

const Header = () => (
  <div className="header">
    <div className="header__title">Sling</div>
    <div className="header__links">
      <Link href="/">
        <div className="header__link">About</div>
      </Link>
      <Link href="/images">
        <div className="header__link">Images</div>
      </Link>
    </div>
    <style jsx>
      {`
        @font-face {
          font-family: 'title';
          src: url('./static/fonts/SedgwickAve-Regular.ttf');
        }

        .header {
          display: flex;
          flex-direction: row;
          align-items: center;
          justify-content: space-between;
          padding: 0 10px;
          height: 40px;
          background-color: #999;
        }

        .header__title {
          font-family: title;
          font-size: 40px;
          margin-bottom: -15px;
        }

        .header__links {
          display: flex;
        }

        .header__link {
          display: flex;
          justify-content: center;
          align-items: center;
          height: 40px;
          width: 100px;
        }

        .header__link:hover {
          background-color: #777;
          cursor: pointer;
        }
      `}
    </style>
  </div>
)

export default Header